// Задание 1. Создание конструктора Student.
console.log('\nЗадание 1. Создание конструктора Student.');

// Конструктор  Student
function Student(name, points) {
  this.name = name;
  this.points = points;
}
Student.prototype.show = function () {
    return console.log('Студент ' + this.name +' набрал ' + this.points + ' баллов');
}


// Задание 2. Создание конструктора StudentList.
console.log('\nЗадание 2. Создание конструктора StudentList.');
function StudentList(group, stdntAndPnt) {
  console.log('\nСоздана группа %s', group);
    
  var items = [];
  obj = this; 
  stdntAndPnt = stdntAndPnt || []; 
    stdntAndPnt.forEach(function (items,i,arr) {
      if(!(i%2)){
        obj.push(new Student(arr[i], arr[i+1]));
      }
    });
    this.group = group;
  }
StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.add = function (name, points) {
  this.push(new Student(name, points));
}
  
// Задание 3. Создать список студентов группы «HJ-2»
var studentsAndPoints = ['Алексей Петров', 0,
                         'Ирина Овчинникова', 60,
                         'Глеб Стукалов', 30,
                         'Антон Павлович', 30,
                         'Виктория Заровская', 30,
                         'Алексей Левенец', 70,
                         'Тимур Вамуш', 30,
                         'Евгений Прочан', 60,
                         'Александр Малов', 0];
var hj2 = new StudentList('HJ-2', studentsAndPoints);

//Добавить несколько новых студентов в группу. Имена и баллы придумайте самостоятельно.
hj2.add('Инга Лукас', 10);
hj2.add('Сергей Чавс', 20);
hj2.add(' Дарья Логан', 20);
console.log(hj2);

//Создать группу «HTML-7» без студентов и сохранить его в переменной html7. После чего добавить в группу несколько студентов. Имена и баллы придумайте самостоятельно
var html7 = new StudentList('HTML-7');
html7.add('Макс Сакс', 10);
html7.add('Венедик Пупкин', 20);
html7.add('Томара Савенка', 30);
console.log(html7);

//Добавить спискам студентов метод show, который выводит информацию о группе в следующем виде:
StudentList.prototype.show = function () {
  console.group('Группа ' + this.group + ' (' + this.length + ' студентов):');
  this.forEach(function (student) {
    student.show();
  });
  console.groupEnd();
}

// Вывести информацию о группах «HJ-2» и «HTML-7». 
hj2.show();
html7.show();

// Задание 7. Перевести одного из студентов из группы «HJ-2» в группу «HTML-7»
html7.push.apply(html7, hj2.splice(4,1));
